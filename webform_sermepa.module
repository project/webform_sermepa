<?php
/**
 * @file
 * Provide sermepa component for webform.
 */

use CommerceRedsys\Payment\Sermepa;
module_load_include('inc', 'webform_sermepa', 'components/sermepa');
module_load_include('inc', 'webform', 'includes/webform.components');
define('WEBFORM_SERMEPA_URL_FINISH', 'sermepa/payment/finish');

/**
 * Implements hook_libraries_info().
 */
function webform_sermepa_libraries_info() {
  $libraries['sermepa'] = array(
    'name' => 'Sermepa/Redsys API',
    'vendor url' => 'https://github.com/CommerceRedsys/sermepa',
    'download url' => 'https://github.com/CommerceRedsys/sermepa/releases',
    'version arguments' => array(
      'file' => 'CHANGELOG.md',
      'pattern' => '@([0-9.]+)@',
      'lines' => 1,
      'cols' => 20,
    ),
    'files' => array(
      'php' => array(
        'src/SermepaException.php',
        'src/SermepaInterface.php',
        'src/Sermepa.php',
      ),
    ),
  );

  return $libraries;
}

/**
 * Helper function to create a Sermepa instance.
 *
 * @return mixed
 *   Initialized \facine\Payment\Sermepa Object, otherwise FALSE.
 */
function webform_sermepa_library_initialize($settings) {
  if (!class_exists('Sermepa')) {
    $library = libraries_load('sermepa');
    if (!$library || empty($library['loaded'])) {
      return FALSE;
    }
  }

  $gateway = new Sermepa(
    $settings['Ds_MerchantName'],
    $settings['Ds_MerchantCode'],
    $settings['Ds_MerchantTerminal'],
    $settings['Ds_MerchantPassword'],
    $settings['mode']
  );

  return $gateway;
}

/**
 * Implements hook_menu().
 */
function webform_sermepa_menu() {
  $items = array();
  $items[WEBFORM_SERMEPA_URL_FINISH . '/%/%'] = array(
    'title' => 'Transaction result',
    'description' => 'Handles callback request from Sermepa',
    'page callback' => '_webform_sermepa_pay_return',
    'page arguments' => array(3,4),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );
  return $items;
}
 
/**
 * Callback function for transaction result menu item.
 */
function _webform_sermepa_pay_return($sid, $nid) {
  if (empty($sid) || empty($nid)) {
    $message = t('Bad feedback url.');
    watchdog('webform_sermepa', $message, array(), WATCHDOG_ERROR);
    return '';
  }

  $node = node_load($nid);
  $settings = array();
  $settings['Ds_MerchantName'] = _webform_sermepa_component_get_field($node, 'sermepa', 'merchant_name');
  $settings['Ds_MerchantCode'] = _webform_sermepa_component_get_field($node, 'sermepa', 'fuc');
  $ds_terminal = _webform_sermepa_component_get_field($node, 'sermepa', 'terminal');
  $settings['Ds_MerchantTerminal'] = str_pad($ds_terminal, 3, '0', STR_PAD_LEFT);
  $settings['Ds_MerchantPassword'] = _webform_sermepa_component_get_field($node, 'sermepa', 'key');
  $settings['mode'] = _webform_sermepa_component_get_field($node, 'sermepa', 'transaction');

  if (!$gateway = webform_sermepa_library_initialize($settings)) {
    $message = t('Fatal error. Unable to process any transaction information.');
    watchdog('webform_sermepa', $message, array(), WATCHDOG_ERROR);
    return '';
  }
  if (!$feedback = $gateway->getFeedback()) {
    $message = t('Bad feedback response data.');
    watchdog('webform_sermepa', $message, array(), WATCHDOG_ERROR);
    return '';
  }
  
  if (_webform_sermepa_check_pay($gateway, $sid, $node)) {
    $message = t('Successful payment. You will receive an email with the details of the service.');
    drupal_set_message($message, 'status', FALSE);
    $url_return = _webform_sermepa_component_get_field($node, 'sermepa', 'url_ok');
    drupal_goto($url_return);
  }
  else {
    $message = t('Unable to process the payment. Please, try it again.');
    drupal_set_message($message, 'error', FALSE);
    $url_return = _webform_sermepa_component_get_field($node, 'sermepa', 'url_ko');
    $options = array('query' => array('nid' => $nid, 'sid' => $sid));
    drupal_goto($url_return, $options);
  }

  $lang = _webform_sermepa_get_form_code_language($feedback['Ds_ConsumerLanguage']);
  $lang_default = language_default('language');
  $prefix = ($lang_default == $lang) ? '' : $lang . '/';
  $translations = translation_path_get_translations($url_return);
  $url_return = drupal_get_path_alias($translations[$lang], $lang);
}

/**
 * Checks if is a valid payment.
 */
function _webform_sermepa_check_pay($gateway, $sid, &$node) {
    $feedback = $gateway->getFeedback();
    $parameters = $gateway->decodeMerchantParameters($feedback['Ds_MerchantParameters']);
    $response_code = $parameters['Ds_Response'];
    $nid = empty($node->nid) ? NULL : $node->nid;
    $cid = NULL;
    $key = '';
    if (!empty($node->webform['components'])) {
      $webform_components = $node->webform['components'];
      foreach ($webform_components as $component) {
        $cid = $component['cid'];
        if ($component['type'] == 'sermepa' && !empty($component['extra']['sermepa']['key'])) {
          $key = $component['extra']['sermepa']['key'];
          break;
        }
      }
    }
    if ($gateway->validSignatures($feedback)) {
      if ($response_code < 100) {
        watchdog('webform_sermepa', 'Order @order_id accepted', array('@order_id' => intval($ds_order)));
        $unpaid = db_select('webform_submitted_data', 'wsd')
          ->fields('wsd', array('data'))
          ->condition('nid', $nid)
          ->condition('sid', $sid)
          ->condition('cid', $cid)
          ->condition('no', 'status')
          ->condition('data', 'unpaid')
          ->execute()
          ->rowCount();
        if ($unpaid && !empty($sid) && !empty($cid) && !empty($nid)) {
          module_load_include('inc', 'webform', 'includes/webform.submissions');
          $submission = webform_get_submission($nid, $sid);
          webform_submission_send_mail($node, $submission);
          db_update('webform_submitted_data')
            ->fields(array(
              'data' => 'paid_tpv',
            ))
            ->condition('sid', $sid, '=')
            ->condition('nid', $nid, '=')
            ->condition('cid', $cid, '=')
            ->condition('no', 'status', '=')
            ->execute();
          db_update('webform_submitted_data')
            ->fields(array(
              'data' => $ds_order,
            ))
            ->condition('sid', $sid, '=')
            ->condition('nid', $nid, '=')
            ->condition('cid', $cid, '=')
            ->condition('no', 'order', '=')
            ->execute();
          db_update('webform_submitted_data')
            ->fields(array(
              'data' => $ds_amount,
            ))
            ->condition('sid', $sid, '=')
            ->condition('nid', $nid, '=')
            ->condition('cid', $cid, '=')
            ->condition('no', 'amount', '=')
            ->execute();
        }
        else {
          watchdog('webform_sermepa', 'Unable to update the payment status to paid.', array(), WATCHDOG_ERROR);
          watchdog('webform_sermepa', 'Unable to send mail with the order information.', array(), WATCHDOG_ERROR);
        }
        return TRUE;
      }
      else {
        $text_code = t(Sermepa::handleResponse($response_code));
        $message = t('Transaction error. Code: @code. @text_code', array('@code' => $response_code, '@text_code' => $text_code));
        drupal_set_message($message, 'error', FALSE);
        watchdog('webform_sermepa', $message, array(), WATCHDOG_ERROR);
      }
      $message = t('Order @order_id DENIED', array('@order_id' => $ds_order));
      drupal_set_message($message, 'error', FALSE);
      watchdog('webform_sermepa', $message, array(), WATCHDOG_ERROR);
      return FALSE;
    }
    else {
      watchdog('webform_sermepa', 'The signatures not match.', array(), WATCHDOG_ERROR);
    }
  
  $message = t('Failed transaction attempt');
  drupal_set_message($message, 'error', FALSE);
  watchdog('webform_sermepa', $message, array(), WATCHDOG_ERROR);
  return FALSE;
}

/**
 * Implements hook_form_alter().
 */
function webform_sermepa_form_alter(&$form, &$form_state, $form_id) {
  if (strpos($form_id, 'webform_client_form_') !== FALSE) {
    if (strpos($form['#action'], 'edit')) {
      $form['actions']['submit']['#value'] = t('Update');
    }
    else {
      $webform_components = $form_state['webform']['component_tree']['children'];
      foreach ($webform_components as $component) {
        if ($component['type'] == 'sermepa') {
          $input_amount = empty($component['extra']['sermepa']['input_amount']) ? '' : $component['extra']['sermepa']['input_amount'];
          $settings = array(
            'amount' => $input_amount,
          );
          drupal_add_js('window.history.forward();', 'inline');
          drupal_add_js(array('webform_sermepa' => $settings), 'setting');
          $form['#node']->webform['emails'] = array();
          $form['actions']['submit']['#ajax']['callback'] = '_webform_sermepa_ajax_callback';
          if (!empty($component['extra']['sermepa']['component_validation'])) {
            $form['#validate'][] = '_webform_sermepa_component_validate';
          }
        }
      }
    }
  }
  elseif ($form_id == 'webform_components_form') {
    $form['add']['add']['#validate'][] = '_webform_sermepa_add_validate';
  }
}

/**
 * Validation function for webform with sermepa component.
 */
function _webform_sermepa_component_validate($form, &$form_state) {
  if ($php_code = _webform_sermepa_component_get_field($form['#node'], 'sermepa', 'component_validation')) {
    if (module_exists('php')) {
      php_eval('<?php ' . $php_code . ' ?>');
    }
    else {
      eval($php_code);
    }
  }
}

/**
 * Validation function for webform_components_form.
 */
function _webform_sermepa_add_validate($form, &$form_state) {
  if ($form_state['values']['add']['type'] == 'sermepa') {
    if (_webform_sermepa_component_exists($form['#node'], 'sermepa')) {
      form_set_error('Sermepa component', t('Already exists a sermepa component in the webform'));
    }
  }
}

/**
 * Ajax callback function for webform submit with sermepa component.
 */
function _webform_sermepa_ajax_callback(&$form, &$form_state) {
  if (!empty($form_state['webform_completed'])) {
    drupal_get_messages();
    $form_tpv = drupal_get_form('_webform_sermepa_form', $form_state, $form['#node']->nid);
    $form_id_selector = '#' . $form['#id'];
    $commands = array();
    $commands[] = ajax_command_invoke($form_id_selector . ' .form-submit', 'attr', array('disabled', 'disabled'));
    $commands[] = ajax_command_remove('#webform-sermepa-form');
    $commands[] = ajax_command_after($form_id_selector, drupal_render($form_tpv));
    $commands[] = ajax_command_invoke('#webform-sermepa-form', 'submit');
    return array('#type' => 'ajax', '#commands' => $commands);
  }
  else {
    return $form;
  }
}

/**
 * Function that builds the sermepa form in order to send to TPV.
 */
function _webform_sermepa_form($form, &$form_state, $webform_state, $nid) {
  global $base_url;
  $sid = $webform_state['values']['details']['sid'];
  $url_callback = WEBFORM_SERMEPA_URL_FINISH . '/' . $sid . '/' . $nid;

  $webform_components = $webform_state['webform']['component_tree']['children'];
  $component_values = array();
  foreach ($webform_components as $component) {
    if ($component['type'] == 'sermepa') {
      $component_values = $component['extra']['sermepa'];
    }
  }

  $settings = array();
  $settings['Ds_MerchantName'] = $component_values['merchant_name'];
  $settings['Ds_MerchantCode'] = $component_values['fuc'];
  $ds_terminal = $component_values['terminal'];
  $settings['Ds_MerchantTerminal'] = str_pad($ds_terminal, 3, '0', STR_PAD_LEFT);
  $settings['Ds_MerchantPassword'] = $component_values['key'];
  $settings['mode'] = $component_values['transaction'];

  if (!$gateway = webform_sermepa_library_initialize($settings)) {
    return FALSE;
  }

  $submitted_data = $webform_state['input']['submitted'];
  $name = _webform_sermepa_key_search($submitted_data, 'name');
  $lastname = _webform_sermepa_key_search($submitted_data, 'lastname');

  $name = empty($name) ? '' : $name;
  $lastname = empty($lastname) ? '' : $lastname;

  if (!empty($component_values['fixed_amount']) && $component_values['fixed_amount'] == 1) {
    $amount = intval($component_values['input_amount']);
  }
  else {
    $component_amount = $component_values['component_amount'];
    $amount = _webform_sermepa_key_search($submitted_data, $component_amount);
    if (strpos($amount, ',') !== FALSE) {
      $amount = floatval(str_replace(',', '.', $amount));
    }
    if ($component_values['currency'] == 978) {
      $amount = round($amount, 2) * 100;
    }
    else {
      $amount = round($amount);
    }
  }

  $order = round(microtime(true) * 100);
  $gateway->setOrder($order)
          ->setAmount($amount)
          ->setCurrency($component_values['currency'])
          ->setMerchantURL($base_url . '/' . $url_callback)
          ->setUrlOK($base_url . '/' . $url_callback)
          ->setUrlKO($base_url . '/' . $url_callback)
          ->setConsumerLanguage($component_values['language'])
          ->setMerchantData(json_encode(array($sid, $nid)))
          ->setTransactionType('0')
          ->setPaymentMethod('T')
          ->setTitular($name . ' ' . $lastname)
          ->setProductDescription($component_values['product_description']);

  $form = array();
  $form['#id'] = 'webform-sermepa-form';
  $form['#action'] = $gateway->getEnvironment();
  $parameters = $gateway->composeMerchantParameters();
  if ($parameters) {
    $form['Ds_SignatureVersion'] = array(
       '#type' => 'hidden',
      '#value' => $gateway->getSignatureVersion(),
    );
    $form['Ds_MerchantParameters'] = array(
      '#type' => 'hidden',
      '#value' => $parameters,
    );
    $form['Ds_Signature'] = array(
      '#type' => 'hidden',
      '#value' => $gateway->composeMerchantSignature(),
    );
  }

  return $form;
}

/**
 * Define components to Webform.
 */
function webform_sermepa_webform_component_info() {
  $components = array();
  $components['sermepa'] = array(
    'label' => 'Sermepa TPV',
    'description' => t('Sermepa submission to replace the standar submit for this form'),
    'features' => array(
      'title_display' => FALSE,
    ),
  );
  return $components;
}

/**
 * Implements hook_permission().
 */
function webform_sermepa_permission() {
  return array(
    'update payment status' => array(
      'title' => t('Update sermepa Payment Status'),
    ),
    'use sermepa testing' => array(
      'title' => t('Use sermepa in Testing Mode'),
    ),
    'use sermepa php' => array(
      'title' => t('Use sermepa PHP'),
      'description' => t('Warning: Give to trusted roles only; this permission has security implications.'),
    ),
  );
}

/**
 * Generate lists with filter components by types.
 */
function _webform_sermepa_filter_node_components(&$node, $types = array()) {
  $webform_components = $node->webform['components'];
  $list = array();
  foreach ($webform_components as $component) {
    if (in_array($component['type'], $types)) {
      $list[$component['form_key']] = $component['name'];
    }
  }
  return $list;
}

/**
 * Checks if a component a type component exists.
 */
function _webform_sermepa_component_exists(&$node, $type) {
  $webform_components = $node->webform['components'];
  foreach ($webform_components as $component) {
    if ($component['type'] == $type) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Gets field value for a component type.
 */
function _webform_sermepa_component_get_field(&$node, $type, $field_name) {
  $webform_components = $node->webform['components'];
  foreach ($webform_components as $component) {
    if ($component['type'] == $type) {
      if (!empty($component['extra']['sermepa'][$field_name])) {
        return $component['extra']['sermepa'][$field_name];
      }
    }
  }
  return NULL;
}

/**
 * Search a key in a fields array.
 */
function _webform_sermepa_key_search($arr, $key) {
  if (is_array($arr)) {
    if (array_key_exists($key, $arr)) {
      if (!is_array($arr[$key])) {
        return $arr[$key];
      }
    }
    else {
      foreach ($arr as $subarr) {
        if ($res = _webform_sermepa_key_search($subarr, $key)) {
          return $res;
        }
      }
    }
  }
}

/**
 * Modifies format for some currency.
 */
function _webform_sermepa_format_amount($amount, $currency) {
  if (strpos($amount, ',') !== FALSE || strpos($amount, '.') !== FALSE) {
    $amount = floatval(str_replace(',', '.', $amount));
    if ($currency == 978) {
      return round($amount, 2) * 100;
    }
    else {
      return round($amount);
    }
  }
  elseif (empty($amount)) {
    form_set_error('webform_sermepa', t('Enter a valid amount'));
  }
}
